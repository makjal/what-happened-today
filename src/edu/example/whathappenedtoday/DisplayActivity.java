package edu.example.whathappenedtoday;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class DisplayActivity extends Activity{

	InputStream inputStream;
	ByteArrayOutputStream byteArrayOutputStream;
	ArrayAdapter<String> mAdapter = null;
	ListView list;
	TextView tv1;
	String line,l12="",ss,tm;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		 setContentView(R.layout.displayy);
		   	
		 
		    tv1 = (TextView)findViewById(R.id.textView1);
		    list= (ListView) findViewById(R.id.listView1);
		    String[] ListItems = new String[]{};
		    
				readTxt1();
		    

	}
	
	 public void readTxt1(){
	        
		 try{

			 Bundle s1 = getIntent().getExtras();
			    String val = s1.getString("mydata");
		       
			    if(val.equals("famousbirthday"))
			    {
			    	inputStream = getResources().openRawResource(R.raw.famousbirthday);
			    		tv1.setText("Famous Bithdays");
			    }
			    else if(val.equals("famousdeaths"))
			    {
			    	inputStream = getResources().openRawResource(R.raw.famousdeaths);	
			    	   tv1.setText("Famous Deaths");
			    }
			    else if(val.equals("historicalevents"))
			    {
			    	inputStream = getResources().openRawResource(R.raw.historicalevents);	
			    	   tv1.setText("Historical Events");
			    }
			    else if(val.equals("computerhistory"))
			    {
			    	inputStream = getResources().openRawResource(R.raw.computerhistory);	
			    	   tv1.setText("Computer And Tech. History");
			    }
			    else if(val.equals("a"))
			    {
			    	inputStream = getResources().openRawResource(R.raw.famousdeaths);	
			    	   tv1.setText("");
			    }
			    else
			    {
			    	finish();
			        System.exit(0);
			    }
			 
			    BufferedReader buffreader = new BufferedReader(new InputStreamReader(inputStream));

		            ArrayList<String> lines = new ArrayList<String>();
		            boolean hasNextLine =true;
		            while (hasNextLine){
		            	
		                line =  buffreader.readLine();
		                //String ss1= line.substring(5, 9);
		      
		                //tv1.setText(sub);
		                tm= readtime();
		                ss= substrng(line);
		              if(ss.equals(tm))
		                {
		                lines.add(line);
		                }
		                hasNextLine = line != null;

		            }
		          //  tv1.setText(dt);
		            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,lines);

		            if(adapter.isEmpty()==true)
		            {
			        	//System.out.println("list is null");
		            	String sdata= "data not found for this day";
		            	lines.add(sdata);
		            	 ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,lines);
		            	list.setAdapter(adapter1);
		            }
		            
		            list.setAdapter(adapter);

		        inputStream.close();
		       

		    }
		     catch(Exception e){
		    	 System.out.println("Exception caught3");
		    	 e.printStackTrace();
		    	 
		     }
		}

    private String readtime() {
		// TODO Auto-generated method stub
    	   SimpleDateFormat dfDate_day= new SimpleDateFormat("MM-dd");
		    String dt="";
		    Calendar c = Calendar.getInstance(); 
		    dt=dfDate_day.format(c.getTime());
		return dt.toString();
	}

	public String substrng(String l11) {
		// TODO Auto-generated method stub
    	try{
    	l12 = l11.substring(5, 10);
    	}catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	return l12.toString();
	}

	public String readTxt(){
        
    	
	   inputStream = getResources().openRawResource(R.raw.famousdeaths);
	   
        byteArrayOutputStream = new ByteArrayOutputStream();
	    
        int i;
     try {
      i = inputStream.read();
      while (i != -1)
         {
          byteArrayOutputStream.write(i);
          i = inputStream.read();
          
	   mAdapter = new ArrayAdapter<String>(this,android.R.layout.list_content, i);
	   list.setAdapter(mAdapter);
         }
         inputStream.close();
     } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
     }
	  
	   
        return byteArrayOutputStream.toString();        
	}

   
}
